# Twenty Fifteen All - Starter Child Theme

__Contributors:__ 

* Tim Russell [Web](http://timrussell.com) / [GitHub](https://github.com/tdavidrussell) / [GitLab](https://gitlab.com/tdavidrussell)    

__Requires:__ 4.8   
__Tested up to:__ 5.0.2   
__License:__ [GPL-2.0+](http://www.gnu.org/licenses/gpl-2.0.html)    
__[Project Page](https://gitlab.com/tdavidrussell/twentyfifteen-all)__   


## Summary
Twenty Fifteen All (starter) Child Theme.  This theme will work as is, though this is not my intention for this theme. 

Download theme install on local development website, duplicate theme and rename. Modified to your needs. 

[Parent Theme](http://wordpress.org/themes/twentyfifteen) REQUIRED!

## Installation


### Upload ###

1. Download the [latest release](https://gitlab.com/tdavidrussell/twentyfifteen-all) from GitLab.
2. Go to the __Appearance &rarr; Themes &rarr; Add New__ screen in your WordPress admin panel and click the __Upload__ tab at the top.
3. Upload the zipped archive.
4. Click the __Activate Theme__ link after installation completes.

### Manual ###

1. Download the [latest release](https://gitlab.com/tdavidrussell/twentyfifteen-all) from GitLab.
2. Unzip the archive.
3. Copy the folder to ' /wp-content/themes/ '.
4. Go to the __Appearance &rarr; Themes__ screen in your WordPress admin panel and click the __Activate__ button on Twenty Fifteen All theme.

Read the Codex for more information about [installing theme manually](https://codex.wordpress.org/Using_Themes).

### Usage ###


## Changelog

See [CHANGELOG](changelog.md).
