<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

define( 'ROTFA_VERSION', '20190710.1' );
define( 'ROTFA_CDIR', get_stylesheet_directory() ); // if child, will be the file path, with out backslash
define( 'ROTFA_CURI', get_stylesheet_uri() ); // URL, no back slash


if ( is_admin() ) {

	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

	/**
	 * Detect plugin. For use in Admin area only.
	 */
	if ( is_plugin_active( 'russell-factor/russell-factor.php' ) ) {
		//plugin is activated
	} else {
		//
		/**
		 * Add the featured image to the first column on post listing page
		 */
		require( ROTFA_CDIR . '/includes/functions-admin-post-images.php' );
		/**
		 * Adds private note the post edit page, just on the admin side,
		 */
		require( ROTFA_CDIR . '/includes/functions-post-private-note.php' );
	}

} else {
	//user side
}
/**
 * 1. Loads the parent and child theme css.
 * 2. Removes things from the header, which gives out toooo much information,
 * 3. Adds style-custom.css if file exists
 * 4. Add login css and js scripts, notice the directory location for the files.
 *      - "/css/custom-login.css"
 *      - '/js/custom-login.js'
 */
require( ROTFA_CDIR . '/includes/functions-header.php' );

/**
 * Loads CSS faster (50% they say) than CCS  @import in child theme style.css
 * @link https://kovshenin.com/2014/child-themes-import/
 * @link http://codex.wordpress.org/Child_Themes#About_.40import_url_in_CSS
 *
 *
 * @return void
 */
function ro_enqueue_style_child_theme_scripts() {

	wp_enqueue_style( 'ro-parent-style', get_template_directory_uri() . '/style.css' );
}

add_action( 'wp_enqueue_scripts', 'ro_enqueue_style_child_theme_scripts' );


/**
 * Change the post-thumbnail (featured image)
 *
 */
function rone_after_theme_setup() {

	set_post_thumbnail_size( 825, 9999, false );
}

add_action( 'after_setup_theme', 'rone_after_theme_setup', 12 );


/**
 * START: Disable responsive images, disable srcset on frontend,
 */
add_filter( 'wp_get_attachment_image_attributes', function ( $attr ) {
	if ( isset( $attr['sizes'] ) ) {
		unset( $attr['sizes'] );
	}
	if ( isset( $attr['srcset'] ) ) {
		unset( $attr['srcset'] );
	}

	return $attr;
}, PHP_INT_MAX );
add_filter( 'wp_calculate_image_sizes', '__return_false', PHP_INT_MAX );
add_filter( 'wp_calculate_image_srcset', '__return_false', PHP_INT_MAX );
remove_filter( 'the_content', 'wp_make_content_images_responsive' );
/**
 * END: Disable Responsive Images
 */

function rone_add_svg_mime_types( $mimes ) {
	$mimes['svg'] = 'image/svg+xml';

	return $mimes;
}

add_filter( 'upload_mimes', 'rone_add_svg_mime_types' );


function danniee001_feed_lifetime( $interval, $url ) {
	//if ( 'http://mysite.org/feed' == $url ) {
	$interval = 0;

	//}

	return $interval;
}

add_filter( 'wp_feed_cache_transient_lifetime', 'danniee001_feed_lifetime', 10, 2 );

?>