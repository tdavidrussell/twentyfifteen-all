Changelog

#### 20190710.1
* Fix: goggle fonts

#### 20180904.1
* Fixed code syntax with private notes
 
#### 20180904.1
* Updated for WordPress 4.9x
* Updated file structure 


#### 2017117.1
* Added: single-resources.php for the RO Resources Plugin

#### 2017117.1
* Changed: Font changed to Lato
* Changed: Featured image size
* Removed: removed responsive images

#### 20160719.1
* Updated: updated theme description
